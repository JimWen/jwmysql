/********************************************************************
	日期:	2013/12/03
	文件: 	JWMysql.cpp JWMysql.h
	作者:	Jim Wen
	版本:	V1.0
	
	功能:	实现对libmysql的一个简单封装,整体模仿PHP中mysql的使用,使用更加方便	
	说明:	必须将项目中文件夹mysql拷贝，并设置好对应的lib、include路径
			将dll加入系统path或放入windows文件夹中,由于数据库连接的特殊
			性，最好使用单例模式接口，否则要注意处理多个连接的并发情况,
			还需要注意的是这个libmysql的函数使用的ASCLL字符串
*********************************************************************/

#include "stdafx.h"
#include "JWMysql.h"

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "libmysql.lib")

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/**
 *功能:初始化和连接mysql
 *参数:传入参数依次为
	   主机地址(默认为本地地址localhost)
	   数据库用户名(默认为root)
	   数据库密码(默认为本地开发为空)
	   数据库端口(默认为mysql的3306)
	   要连接的数据库名(默认为连接默认数据库,一般要设置这个参数)
	   后两个参数参见手册，一般简单应用不用
 *返回:无
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
JWMysql::JWMysql( const char *host/*="localhost"*/, 
				  const char *user/*="root"*/, 
				  const char *passwd/*=""*/, 
				  const unsigned int port/*=3306*/, 
				  const char *db/*=NULL*/, 
				  const char *unix_socket/*=NULL*/, 
				  const unsigned long client_flag/*=0*/ )
{
	ASSERT(0 == mysql_library_init(0, NULL, NULL));
	ASSERT(NULL != mysql_init(&m_mysql_link));

	m_mysql_pRes = NULL;
	m_num_rows = 0;
	m_num_fields = 0;
	bIsConnect = FALSE;

	//如果输入主机名则连接Mysql
	if (lstrlen(host) != 0 && 
		NULL != mysql_real_connect(&m_mysql_link, host, user, passwd, db, port, unix_socket, client_flag))
	{
		bIsConnect = TRUE;
	}
}

/**
 *功能:析构函数，先释放结果集，然后断开连接
 *参数:无
 *返回:无
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
JWMysql::~JWMysql()
{
	if (m_num_fields > 0)
	{
		mysql_free_result(m_mysql_pRes);
		m_num_fields = 0;
	}
	if (bIsConnect == TRUE)
	{
		mysql_close(&m_mysql_link);
		bIsConnect = FALSE;
	}
	mysql_library_end();
}

/**
 *功能:连接Mysql，如果之前连接了自动断开重连
 *参数:和构造函数一样
 *返回:连接成功返回TRUE,失败返回FALSE
 *其他:2014/01/09 By Jim Wen Ver1.0
**/
BOOL JWMysql::Connnet( const char *host/*="localhost"*/, 
					   const char *user/*="root"*/, 
					   const char *passwd/*=""*/, 
					   const unsigned int port/*=3306*/, 
					   const char *db/*=NULL*/, 
					   const char *unix_socket/*=NULL*/, 
					   const unsigned long client_flag/*=0*/ )
{
	//断开之前的连接
	if (TRUE == bIsConnect)
	{
		Disconnect();
	}

	//如果输入主机名则连接Mysql
	if (lstrlen(host) != 0 && 
		NULL != mysql_real_connect(&m_mysql_link, host, user, passwd, db, port, unix_socket, client_flag))
	{
		bIsConnect = TRUE;
	}
	else
	{
		bIsConnect = FALSE;
	}

	return bIsConnect;
}

/**
 *功能:断开Mysql连接
 *参数:无
 *返回:无
 *其他:2014/01/09 By Jim Wen Ver1.0
**/
void JWMysql::Disconnect()
{
	if (m_num_fields > 0)
	{
		mysql_free_result(m_mysql_pRes);
		m_num_fields = 0;
	}
	if (bIsConnect == TRUE)
	{
		mysql_close(&m_mysql_link);
		bIsConnect = FALSE;
	}
}

/**
 *功能:是否已经连接Mysql
 *参数:无
 *返回:已经连接返回TRUE,否则返回FALSE
 *其他:2014/01/09 By Jim Wen Ver1.0
**/
BOOL JWMysql::IsConnect()
{
	return this->bIsConnect;
}

/**
 *功能:单例模式接口,现在的限制在于
 *参数:无
 *返回:JWMysql对象指针,直接使用,先连接指定数据库
 *其他:2014/01/09 By Jim Wen Ver1.0
**/
JWMysql* JWMysql::GetInstance()
{
	static JWMysql instance("");//默认不连接数据库
	return &instance;
}

/**
 *功能:执行数据库相关操作,将INSERT,UPDATE,DROP,SELECT等封装在一个函数中
	   1.
	   如果是要获得结果集的SELECT则会在内部获取结果集，其效果就是调用该
	   函数后，直接调用FetchRow、FetchField等函数获得结果集相关参数，并
	   且上次执行SELECT返回的结果集在下次执行时会自动释放，直接使用即可
	   不用关心结果集的释放问题，调用该函数后可使用GetFieldNum和GetRowNum
	   获取结果集的字段数和记录数
	   2.
	   如果是不获取结果集的操作则会直接对数据库做相关操作，可使用GetRowNum
	   查看影响的记录数目(如INSERT,UPDATE,DROP,DELETE等)
 *参数:szQuery--SQL语句
 *返回:成功--TRUE，有错误FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
BOOL JWMysql::Query(const char *szQuery)
{
	//释放上次的查询结果集
	if (m_num_fields > 0)
	{
		mysql_free_result(m_mysql_pRes);
		m_num_fields = 0;
	}

	//这里的用法参见手册上的函数mysql_field_count()内容[笔者用中文手册5.1]
	if(0 != mysql_real_query(&m_mysql_link, szQuery, lstrlen(szQuery)))
	{
		return FALSE;
	}
	else
	{
		m_mysql_pRes = mysql_store_result(&m_mysql_link);

		if (m_mysql_pRes)  // there are rows
		{
			m_num_fields = mysql_num_fields(m_mysql_pRes);// retrieve rows, then call mysql_free_result(result)
			m_num_rows = mysql_num_rows(m_mysql_pRes);
			return TRUE;
		}
		else  // mysql_store_result() returned nothing; should it have?
		{
			if(mysql_field_count(&m_mysql_link) == 0)
			{
				// query does not return data
				// (it was not a SELECT)
				m_num_fields = 0;
				m_num_rows = mysql_affected_rows(&m_mysql_link);
				return TRUE;
			}
			else // mysql_store_result() should have returned data
			{
				//fprintf(stderr, "Error: %s\n", mysql_error(&m_mysql_link));

				//发生错误时，清空上次查询数据
				if (m_num_fields > 0)
				{
					mysql_free_result(m_mysql_pRes);
					m_num_fields = 0;
				}
				m_num_rows = 0;

				return FALSE;
			}
		}
	}
}

/**
 *功能:选择要操作的数据库,设为默认,在此之后的操作均默认在该数据库中
 *参数:szDb--要操作的数据库名
 *返回:成功--TRUE，有错误FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
BOOL JWMysql::SelectDb( const char *szDb )
{
	return 0 == mysql_select_db(&m_mysql_link, szDb) ? TRUE : FALSE;
}

/**
 *功能:设置要相应的字符集,设为默认,在此之后的操作均默认为该字符集
 *参数:szName--要设置的字符集名(常见为utf8,gbk,操作中文一般用gbk)
 *返回:成功--TRUE，有错误FALSE
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
BOOL JWMysql::SetCharacterSet(const char *szName )
{
	return 0 == mysql_set_character_set(&m_mysql_link, szName) ? TRUE : FALSE;
}

/**
 *功能:对要插入的mysql数据库中的带有\' \"等mysql中定义的转义字符进行转义
	   例如\'转义为\\\'插入mysql后才能为\'
 *参数:szFrom--要转义的字符串
 *返回:转义成功后的字符串
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
char * JWMysql::Escape(const char *szFrom)
{
	mysql_real_escape_string(&m_mysql_link, m_szBuffer, szFrom, lstrlen(szFrom));

	return m_szBuffer;
}

/**
 *功能:获取刚刚一次SELECT Query的结果集的一条记录，可循环调用，参考demo用法
 *参数:无
 *返回:MYSQL_ROW形式的一条记录数据
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
MYSQL_ROW JWMysql::FetchRow()
{
	if (m_num_fields > 0)
	{
		return mysql_fetch_row(m_mysql_pRes);
	}
	else
	{
		return NULL ;
	}
}

/**
 *功能:获取刚刚一次SELECT Query的结果集的一个字段，可循环调用，参考demo用法
 *参数:无
 *返回:MYSQL_FIELD *形式的一字段数据
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
MYSQL_FIELD * JWMysql::FetchField()
{
	if (m_num_fields > 0)
	{
		return mysql_fetch_field(m_mysql_pRes);
	}
	else
	{
		return NULL ;
	}
}

/**
 *功能:获取刚刚一次SELECT Query的结果集的指定字段数据，从0开始算Index
 *参数:fieldnr--字段的Index
 *返回:MYSQL_FIELD *形式的一字段数据
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
MYSQL_FIELD * JWMysql::FetchFieldAt(const unsigned int nFieldIndex)
{
	if (m_num_fields > 0)
	{
		return mysql_fetch_field_direct(m_mysql_pRes, nFieldIndex);
	}
	else
	{
		return NULL ;
	}
}

/**
 *功能:获取刚刚一次SELECT Query的结果集的所有字段数据
 *参数:无
 *返回:MYSQL_FIELD *形式的所有字段数据
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
MYSQL_FIELD * JWMysql::FetchFields()
{
	if (m_num_fields > 0)
	{
		return mysql_fetch_fields(m_mysql_pRes);
	}
	else
	{
		return NULL ;
	}
}

/**
 *功能:获得mysql的错误信息
 *参数:无
 *返回:字符串形式的错误信息
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
const char * JWMysql::GetErrorInfo()
{
	return mysql_error(&m_mysql_link);
}

/**
 *功能:获得mysql的错误编号
 *参数:无
 *返回:0表示没有错误,非0表示错误编号
 *其他:2014/01/09 By Jim Wen Ver1.0
**/
unsigned int JWMysql::GetErrorCode()
{
	return mysql_errno(&m_mysql_link);
}

/**
 *功能:获得SELELCT到的字段数目
 *参数:无
 *返回:字段数目
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
unsigned long JWMysql::GetFiledNum()
{
	return this->m_num_fields;
}

/**
 *功能:获得SELECT得到的记录数和UPDATE等操作影响的记录数目
 *参数:无
 *返回:记录数目
 *其他:2013/12/03 By Jim Wen Ver1.0
**/
__int64 JWMysql::GetRowNum()
{
	return this->m_num_rows;
}
