#ifndef JWMYSQL_H_H_H
#define JWMYSQL_H_H_H

#include <winsock.h>//一定要在mysql.h前面引入该文件
#include <mysql.h>

class JWMysql  
{
public:
	JWMysql(const char *host="localhost",
		    const char *user="root",
			const char *passwd="",
			const unsigned int port=3306, 
			const char *db=NULL,
			const char *unix_socket=NULL,
			const unsigned long client_flag=0);
	virtual ~JWMysql();
	BOOL Connnet(const char *host="localhost",
				 const char *user="root",
				 const char *passwd="",
				 const unsigned int port=3306, 
				 const char *db=NULL,
				 const char *unix_socket=NULL,
				 const unsigned long client_flag=0);
	void Disconnect();
	BOOL IsConnect();
	BOOL Query(const char *szQuery); 
	MYSQL_ROW FetchRow();
	MYSQL_FIELD *FetchField();
	MYSQL_FIELD *FetchFieldAt(const unsigned int nFieldIndex);
	MYSQL_FIELD *FetchFields();
	BOOL SelectDb(const char *szDb); 
	BOOL SetCharacterSet(const char *szName);
	char *Escape(const char *szFrom);
	const char *GetErrorInfo();
	unsigned int GetErrorCode() ;
	unsigned long GetFiledNum();
	__int64 GetRowNum();

	static JWMysql* GetInstance();

private:
	BOOL bIsConnect;
	MYSQL m_mysql_link;
	MYSQL_RES *m_mysql_pRes;
	char m_szBuffer[256];
	unsigned long m_num_fields;
	my_ulonglong m_num_rows;
};

#endif
